package main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import main.captions.CaptionService;
import main.data.InputHandler;
import main.data.InputScanner;

@Component
public class StartMenu {
	@Autowired
	private CaptionService captionService;
	@Autowired
	private InputScanner inputScanner;
	@Autowired
	private InputHandler inputHandler;

	public final static String QUIT_OPTION_ID = "5";

	public void start() {
		String userInput = selectFromMenu();

		while (!userInput.equals(QUIT_OPTION_ID)) {
			inputHandler.handleInput(userInput);
			userInput = selectFromMenu();
		}

	}

	private String selectFromMenu() {
		printMenu();
		String userInput = inputScanner.getInput();
		return userInput;
	}

	private void printMenu() {
		System.out.println(captionService.getCaption("menu.choices"));
	}
}
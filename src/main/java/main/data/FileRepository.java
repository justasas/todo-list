package main.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import main.Constants;
import main.Main;
import main.tasks.ToDoTask;

@Service
public class FileRepository implements Repository {

	@Override
	public boolean addTask(ToDoTask task) {
		return true;
	}

	@Override
	public boolean removeTask(int id) {
		return true;
	}

	@Override
	public boolean updateTask(ToDoTask task) {
		return true;
	}

	private void updateTasksFile(List<ToDoTask> tasks) {

		File originalFile = new File(Constants.RESOURCES_DIR + Main.VARIABLES.get(Constants.FILE_TASKS_NAME_KEY));
		if (!originalFile.delete()) {
			System.out.println("Could not delete file");
			return;
		}

		File newOriginalFile = new File(Constants.RESOURCES_DIR + Main.VARIABLES.get(Constants.FILE_TASKS_NAME_KEY));
		writeTasksToFile(newOriginalFile, tasks);
	}

	private void writeTasksToFile(File file, List<ToDoTask> tasks) {
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new FileWriter(file));

			String line = null;

			for (ToDoTask task : tasks) {
				final String delimeter = Main.VARIABLES.get(Constants.FILE_TASKS_DELIMETER);
				String startTime = Constants.DATE_FORMAT.format(task.getStartTime());
				line = task.getType() + delimeter + task.getName() + delimeter + task.getLocation() + delimeter
						+ startTime + delimeter + task.getDuration();

				pw.println(line);
			}
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public ToDoTask getTask(int id) {
		return getAllTasks().get(id);
	}

	@Override
	public List<ToDoTask> getAllTasks() {
		List<ToDoTask> ret = new ArrayList<ToDoTask>();

		try (BufferedReader br = new BufferedReader(new FileReader(
				new File(Constants.RESOURCES_DIR + Main.VARIABLES.get(Constants.FILE_TASKS_NAME_KEY))))) {
			String line;
			while ((line = br.readLine()) != null) {

				String[] splitted = line.split(",");

				String type = splitted[0];
				ToDoTask toDoTask = ToDoTask.CREATE(type);

				toDoTask.setName(splitted[1]);
				toDoTask.setLocation(splitted[2]);

				String dateFromFile = splitted[3];
				toDoTask.setStartTime(dateFromFile);

				toDoTask.setDuration(splitted[4]);

				ret.add(toDoTask);
			}

			return ret;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

}

package main.data;

import java.util.Scanner;

import org.springframework.stereotype.Service;

@Service
public class ConsoleInputScanner implements InputScanner {
	private Scanner scanner = null;

	@Override
	public String getInput() {
		String ret = null;

		if (scanner == null)
			scanner = new Scanner(System.in);
		ret = scanner.nextLine();

		return ret;
	}
}

package main.data;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import main.startMenu.AddTaskStartMenuChoice;
import main.startMenu.DeleteTaskStartMenuChoice;
import main.startMenu.PrintTasksByCategoryStartMenuChoice;
import main.startMenu.PrintTasksStartMenuChoice;
import main.startMenu.QuitStartMenuChoice;
import main.startMenu.StartMenuChoice;
import main.startMenu.UpdateTaskStartMenuChoice;

@Component
public class InputHandler {

	@Autowired
	AddTaskStartMenuChoice addTaskStartMenuChoice;
	@Autowired
	PrintTasksByCategoryStartMenuChoice printTasksByCategoryStartMenuChoice;
	@Autowired
	PrintTasksStartMenuChoice printTasksStartMenuChoice;
	@Autowired
	DeleteTaskStartMenuChoice deleteTaskStartMenuChoice;
	@Autowired
	QuitStartMenuChoice quitStartMenuChoice;
	@Autowired
	UpdateTaskStartMenuChoice updateTaskStartMenuChoice;

	private Map<String, StartMenuChoice> getStartMenuChoicesToExecutors() {

		Map<String, StartMenuChoice> ret = new HashMap<>();

		ret.put("1", addTaskStartMenuChoice);
		ret.put("2", printTasksStartMenuChoice);
		ret.put("3", printTasksByCategoryStartMenuChoice);
		ret.put("4", deleteTaskStartMenuChoice);
		ret.put("5", quitStartMenuChoice);
		ret.put("6", updateTaskStartMenuChoice);

		return ret;
	}

	public void handleInput(String userInput) {
		final StartMenuChoice startMenuChoice = getStartMenuChoicesToExecutors().get(userInput);

		if (startMenuChoice != null) {
			startMenuChoice.execute();
		} else {
			System.out.println("Bad Menu Choice!");
		}
	}

}
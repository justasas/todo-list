package main.data;

public interface InputScanner {
	public String getInput();
}
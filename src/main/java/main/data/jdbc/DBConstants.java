package main.data.jdbc;

public class DBConstants {
	public static final String JDBC_DRIVER = "org.postgresql.Driver";
	public static final String DB_URL = "jdbc:postgresql://localhost/postgres";

	public static final String DB_USER = "postgres";
	public static final String DB_PASS = "postgres";
}
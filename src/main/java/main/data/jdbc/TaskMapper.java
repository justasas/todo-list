package main.data.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import main.tasks.ToDoTask;

public class TaskMapper implements RowMapper<ToDoTask> {

	@Override
	public ToDoTask mapRow(ResultSet rs, int arg1) throws SQLException {
		return ToDoTask.CREATE(rs.getInt("id"), rs.getString("type"), rs.getString("name"), rs.getString("duration"),
				rs.getDate("start_time"), rs.getString("location"), rs.getBoolean("already_notified"));
	}

}
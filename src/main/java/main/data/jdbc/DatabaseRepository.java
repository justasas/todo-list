package main.data.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import main.data.Repository;
import main.tasks.Attendee;
import main.tasks.MeetingTask;
import main.tasks.TaskTypeEnum;
import main.tasks.ToDoTask;

@org.springframework.stereotype.Repository
public class DatabaseRepository implements Repository {

	static final String INSERT_TASK_QUERY = "INSERT INTO task(name, start_time, duration, location, type, already_notified) VALUES(?,?,?,?,?,?)";
	static final String INSERT_ATTENDEE = "INSERT INTO attendee(name) VALUES(?)";
	static final String INSERT_TASK_ATTENDEE = "INSERT INTO task_attendee(task_id, attendee_id) VALUES(?,?)";

	static final String UPDATE_TASK_QUERY = "UPDATE task SET name = ?, start_time = ?, duration = ?, location = ?, type = ?, already_notified = ? where id = ?";
	static final String UPDATE_ATTENDEE = "UPDATE attendee SET name = ? where id = ?";

	static final String REMOVE_TASK_QUERY = "DELETE FROM task WHERE id = ?";
	static final String REMOVE_TASK_ATTENDEE = "DELETE FROM task_attendee WHERE task_id = ? AND attendee_id = ?";
	static final String REMOVE_ALL_TASK_ATTENDEES = "DELETE FROM task_attendee WHERE task_id = ?";

	static final String FIND_TASK_ATTENDEES_IDS_QUERY = "select attendee_id from task_attendee where task_id = ?";
	// static final String FIND_TASK_ATTENDEES = "select * from attendee where
	// attendee_id = ?";
	static final String FIND_ALL_TASKS_QUERY = "SELECT * FROM task";
	static final String FIND_TASK_ATTENDEES = "SELECT * FROM attendee WHERE id IN (select attendee_id from task_attendee where task_id = ?)";

	static final String FIND_TASK_QUERY = "SELECT * FROM task where id = ?";

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public DatabaseRepository() {
		try {
			Class.forName(DBConstants.JDBC_DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public Connection getConnection() {
		try {
			return DriverManager.getConnection(DBConstants.DB_URL, DBConstants.DB_USER, DBConstants.DB_PASS);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public boolean removeTaskJDBC(int id) {
		PreparedStatement stmt;
		try (Connection conn = getConnection()) {
			stmt = conn.prepareStatement(REMOVE_TASK_QUERY);
			stmt.setInt(1, id);
			if (stmt.executeUpdate() == 1)
				return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean removeTask(int id) {
		if (this.jdbcTemplate.update(REMOVE_TASK_QUERY, Long.valueOf(id)) == 1)
			return true;

		return false;
	}

	@Override
	public ToDoTask getTask(int id) {
		PreparedStatement stmt;
		try (Connection conn = getConnection()) {
			stmt = conn.prepareStatement(FIND_TASK_QUERY);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			if (rs.next()) {

				ToDoTask toDoTask = createTaskFromResultSet(rs);

				return toDoTask;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	private ToDoTask createTaskFromResultSet(ResultSet rs) {
		int id;
		try {
			id = rs.getInt("id");
			String name = rs.getString("name");
			Date startTime = new Date(rs.getTimestamp("start_time").getTime());
			String location = rs.getString("location");
			String duration = rs.getString("duration");
			String type = rs.getString("type");
			boolean alreadyNotified = rs.getBoolean("already_notified");

			ToDoTask toDoTask = ToDoTask.CREATE(id, type, name, duration, startTime, location, alreadyNotified);
			setTaskAttendees(id, type, toDoTask);

			return toDoTask;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	// @Override
	public ToDoTask getTaskJDBCtemplate(int id) {
		return this.jdbcTemplate.queryForObject(FIND_TASK_QUERY, new Object[] { id }, new TaskMapper());
	}

	// @Override
	public List<ToDoTask> getAllTasksJDBCtemplate() { // TODO: get meetings
		return this.jdbcTemplate.query(FIND_ALL_TASKS_QUERY, new TaskMapper());
	}

	@Override
	public List<ToDoTask> getAllTasks() {
		Statement stmt;
		try (Connection conn = getConnection()) {

			List<ToDoTask> ret = new ArrayList<ToDoTask>();

			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(FIND_ALL_TASKS_QUERY);

			while (rs.next()) {
				ToDoTask toDoTask = createTaskFromResultSet(rs);
				ret.add(toDoTask);
			}
			return ret;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	private void setTaskAttendees(int id, String type, ToDoTask toDoTask) throws SQLException {
		if (type.equals(TaskTypeEnum.meeting.toString())) {
			((MeetingTask) toDoTask).setAttendees(findTaskAttendees(id));
		}
	}

	private List<Attendee> findTaskAttendees(int id) throws SQLException {
		try (Connection conn = getConnection()) {
			PreparedStatement stmt2 = conn.prepareStatement(FIND_TASK_ATTENDEES);
			stmt2.setInt(1, id);
			ResultSet rs2 = stmt2.executeQuery();

			List<Attendee> attendees = new ArrayList<Attendee>();

			while (rs2.next()) {
				Attendee attendee = new Attendee(rs2.getString("name"));
				attendee.setId(rs2.getInt("id"));
				attendees.add(attendee);
			}
			return attendees;
		}
	}

	private Timestamp toSqlTimeStamp(java.util.Date date) {
		if (date == null) {
			return null;
		}
		return new java.sql.Timestamp(date.getTime());
	}

	@Override
	public boolean addTask(ToDoTask task) {
		PreparedStatement stmt;

		Connection conn = getConnection();

		try {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(INSERT_TASK_QUERY, Statement.RETURN_GENERATED_KEYS);

			setTaskStatementInformation(task, stmt);
			int rs = stmt.executeUpdate();

			if (stmt.getGeneratedKeys().next()) {
				int generatedTaskId = stmt.getGeneratedKeys().getInt("id");
				task.setId(generatedTaskId);
				if (rs == 1) {
					if (task.getType().equals(TaskTypeEnum.meeting.toString())) {
						List<Attendee> taskAttendees = ((MeetingTask) task).getAttendees();

						if (!insertAttendees(task, conn, taskAttendees))
							return false;
					}
				}

				conn.commit();
				conn.close();

				return true;

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			conn.rollback();
			conn.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		return false;
	}

	private boolean insertAttendees(ToDoTask task, Connection conn, List<Attendee> taskAttendees) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(INSERT_ATTENDEE, Statement.RETURN_GENERATED_KEYS);
		for (Attendee taskAttendee : taskAttendees) {
			stmt.setString(1, taskAttendee.getName());
			stmt.executeUpdate();
			if (stmt.getGeneratedKeys().next()) {
				int generatedAttendeeId = stmt.getGeneratedKeys().getInt("id");
				taskAttendee.setId(generatedAttendeeId);

				if (!insertTaskAttendee(task, taskAttendee, conn))
					return false;
			} else
				return false;
		}

		return true;
	}

	private boolean insertTaskAttendee(ToDoTask task, Attendee taskAttendee, Connection conn) throws SQLException {

		PreparedStatement stmt2 = conn.prepareStatement(INSERT_TASK_ATTENDEE);
		stmt2.setInt(1, task.getId());
		stmt2.setInt(2, taskAttendee.getId());

		if (stmt2.executeUpdate() == 1)
			return true;

		return false;
	}

	private boolean deleteTaskAttendee(int taskId, int attendeeId) throws SQLException {

		try (Connection conn = getConnection()) {
			PreparedStatement stmt2 = conn.prepareStatement(REMOVE_TASK_ATTENDEE);
			stmt2.setInt(1, taskId);
			stmt2.setInt(2, attendeeId);

			if (stmt2.executeUpdate() == 1)
				return true;
		}

		return false;
	}

	private boolean deleteTaskAttendees(ToDoTask task, Connection conn) throws SQLException {

		PreparedStatement stmt2 = conn.prepareStatement(REMOVE_ALL_TASK_ATTENDEES);
		stmt2.setInt(1, task.getId());
		stmt2.executeUpdate();
		return true;
	}

	private void setTaskStatementInformation(ToDoTask task, PreparedStatement stmt) throws SQLException {
		stmt.setString(1, task.getName());
		stmt.setTimestamp(2, toSqlTimeStamp(task.getStartTime()));
		stmt.setString(3, task.getDuration());
		stmt.setString(4, task.getLocation());
		stmt.setString(5, task.getType());
		stmt.setBoolean(6, task.isAlreadyNotified());
	}

	@Override
	public boolean updateTask(ToDoTask task) {
		PreparedStatement stmt;
		Connection conn = getConnection();

		try {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(UPDATE_TASK_QUERY);

			setTaskStatementInformation(task, stmt);
			stmt.setInt(7, task.getId());

			int rs = stmt.executeUpdate();

			if (rs == 1) {

				if (!updateAttendees(task, conn)) {
					conn.commit();
					conn.close();

					return true;
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			conn.rollback();
			conn.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		return false;
	}

	private boolean updateAttendees(ToDoTask task, Connection conn) throws SQLException {
		boolean attendeesUpdateFailed = false;

		if (task.getType().equals(TaskTypeEnum.meeting.toString())) {

			List<Attendee> taskAttendeesInRAM = ((MeetingTask) task).getAttendees();
			List<Attendee> taskAttendeesInDatabase = findTaskAttendees((task.getId()));

			List<Attendee> taskAttendeesFoundInDbButNotInRAM = new ArrayList<Attendee>(taskAttendeesInDatabase);
			List<Attendee> taskAttendeesFoundInRAMButNotInDB = new ArrayList<Attendee>(taskAttendeesInRAM);

			taskAttendeesFoundInDbButNotInRAM.removeAll(taskAttendeesInRAM);
			taskAttendeesFoundInRAMButNotInDB.removeAll(taskAttendeesInDatabase);

			if (!insertAttendees(task, conn, taskAttendeesFoundInRAMButNotInDB))
				attendeesUpdateFailed = true;

			for (Attendee taskAttendeeFoundInDbButNotInRAM : taskAttendeesFoundInDbButNotInRAM) {
				if (!deleteTaskAttendee(task.getId(), taskAttendeeFoundInDbButNotInRAM.getId())) {
					attendeesUpdateFailed = true;
					break;
				}
			}
		}

		return attendeesUpdateFailed;
	}
}

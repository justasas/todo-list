package main.data;

import java.util.List;

import main.tasks.ToDoTask;

public interface Repository {

	boolean removeTask(int id);

	ToDoTask getTask(int id);

	List<ToDoTask> getAllTasks();

	boolean addTask(ToDoTask task);

	boolean updateTask(ToDoTask task);

}
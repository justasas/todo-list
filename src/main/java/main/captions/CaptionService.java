package main.captions;

public interface CaptionService {

	String getCaption(String key);

}

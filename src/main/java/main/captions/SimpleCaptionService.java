package main.captions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import main.Constants;
import main.StartMenu;

@Service
public class SimpleCaptionService implements CaptionService {
	final private static Map<String, String> captionMap = getDefaultCaptions();
	final private static String CAPTIONS_FILE_KEY_TO_VALUE_SPLIT_SYMBOL = "=";
	final private static String CAPTIONS_FILE_PATH = Constants.RESOURCES_DIR + "captions.txt";

	private static Map<String, String> getDefaultCaptions() {
		Map<String, String> captions = new HashMap<String, String>();

		captions.put("menu.choices", "1. Add task\n" + "2. Print out tasks\n" + "3. Print out tasks by category\n"
				+ "4. Delete a task\n" + StartMenu.QUIT_OPTION_ID + ". Quit\n" + "6. Update task");

		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(CAPTIONS_FILE_PATH)));

			String line;
			while ((line = br.readLine()) != null) {
				String[] keyValue = line.split(CAPTIONS_FILE_KEY_TO_VALUE_SPLIT_SYMBOL);
				captions.put(keyValue[0], keyValue[1]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return captions;
	}

	@Override
	public String getCaption(String key) {
		return captionMap.get(key);
	}
}
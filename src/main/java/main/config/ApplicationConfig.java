package main.config;

import java.util.Objects;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import main.captions.CaptionService;
import main.captions.SimpleCaptionService;
import main.captions.properties.CaptionProperties;
import main.data.FileRepository;
import main.data.Repository;
import main.data.RepositoryProperties;
import main.data.jdbc.DBConstants;
import main.data.jdbc.DatabaseRepository;

@Configuration
public class ApplicationConfig {

	@Autowired
	private CaptionProperties captionProperties;

	@Autowired
	private RepositoryProperties repositoryProperties;

	@Bean
	public CaptionService captionService() {
		final CaptionType type = CaptionType.valueOf(captionProperties.getType());

		if (Objects.equals(type, CaptionType.SIMPLE)) {
			return new SimpleCaptionService();
		}

		return new SimpleCaptionService();
	}

	@Bean
	public JdbcTemplate jdbcTemplate(DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}

	@Bean
	public Repository repository() {
		final RepositoryType type = RepositoryType.valueOf(repositoryProperties.getType());

		if (Objects.equals(type, RepositoryType.DATABASE)) {
			return new DatabaseRepository();
		}

		if (Objects.equals(type, RepositoryType.FILE)) {
			return new FileRepository();
		}

		return new DatabaseRepository();
	}

	@Bean
	public DataSource dataSource() {
		org.springframework.jdbc.datasource.DriverManagerDataSource dataSource = new org.springframework.jdbc.datasource.DriverManagerDataSource();
		dataSource.setDriverClassName(DBConstants.JDBC_DRIVER);
		dataSource.setUrl(DBConstants.DB_URL);
		dataSource.setUsername(DBConstants.DB_USER);
		dataSource.setPassword(DBConstants.DB_PASS);

		return dataSource;
	}

	private enum CaptionType {
		FILE, SIMPLE
	}

	private enum RepositoryType {
		FILE, DATABASE
	}
}

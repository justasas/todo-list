package main.home;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import main.tasks.TasksService;
import main.tasks.ToDoTask;

@Controller
public class HomeController {

	@Autowired
	private TasksService tasksService;

	@ModelAttribute("allTasks")
	public List<ToDoTask> allTasks() {
		return tasksService.getTasks();
	}

	@RequestMapping("/")
	public String home() {
		return "index";
	}
}

package main.startMenu;

import org.springframework.stereotype.Component;

import main.tasks.Attendee;
import main.tasks.MeetingTask;
import main.tasks.TaskTypeEnum;
import main.tasks.ToDoTask;

@Component
public class AddTaskStartMenuChoice extends StartMenuChoice {

	@Override
	void executeSpecific() {
		ToDoTask toDoTask = getToDoTaskInformation();
		tasksService.addTask(toDoTask);
	}

	private ToDoTask getToDoTaskInformation() {
		System.out.println("Enter \"reminder\" to CREATE REMINDER, meeting to CREATE MEETING, reserve to RESERVE TIME");
		final String taskType = inputScanner.getInput();
		ToDoTask toDoTask = ToDoTask.CREATE(taskType);

		System.out.println(captionService.getCaption("menu.input.handler.add.task.name"));
		toDoTask.setName(inputScanner.getInput());

		System.out.println(captionService.getCaption("menu.input.handler.add.task.location"));
		toDoTask.setLocation(inputScanner.getInput());

		System.out.println(captionService.getCaption("menu.input.handler.add.task.start.date"));
		toDoTask.setStartTime(inputScanner.getInput());

		System.out.println(captionService.getCaption("menu.input.handler.add.task.duration"));
		toDoTask.setDuration(inputScanner.getInput());

		if (taskType.equals(TaskTypeEnum.meeting.toString())) {
			addAttendees(toDoTask);
		}

		return toDoTask;
	}

	public void addAttendees(ToDoTask toDoTask) {
		while (true) {
			System.out.println(captionService.getCaption("menu.input.handler.add.task.attendee"));
			final String attendeeName = inputScanner.getInput();
			if (attendeeName.isEmpty())
				break;
			else {
				Attendee attendee = new Attendee(attendeeName);
				((MeetingTask) toDoTask).addAttendee(attendee);
			}
		}
	}
}
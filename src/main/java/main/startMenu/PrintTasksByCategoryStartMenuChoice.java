package main.startMenu;

import java.util.List;

import org.springframework.stereotype.Component;

import main.tasks.TaskTypeEnum;
import main.tasks.ToDoTask;

@Component
public class PrintTasksByCategoryStartMenuChoice extends StartMenuChoice {

	public PrintTasksByCategoryStartMenuChoice() {
	}

	@Override
	void executeSpecific() {
		System.out.println("Input category name (" + TaskTypeEnum.reminder + ", " + TaskTypeEnum.meeting + ", "
				+ TaskTypeEnum.reserveTime);

		final String input = inputScanner.getInput();

		System.out.println("listing tasks by type = " + input + ":");

		List<ToDoTask> tasksByType = tasksService.findByType(input);

		tasksService.printOutTasks(tasksByType);
	}
}
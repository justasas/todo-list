package main.startMenu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import main.tasks.MeetingTask;
import main.tasks.TaskTypeEnum;
import main.tasks.ToDoTask;

@Component
public class UpdateTaskStartMenuChoice extends StartMenuChoice {

	@Autowired
	AddTaskStartMenuChoice addTaskStartMenuChoice;

	private void updateTask() {

		System.out.println("Input item id to update");

		String taskId = inputScanner.getInput();
		ToDoTask task = tasksService.getTask(Integer.valueOf(taskId));

		System.out.print("Update  1. type 2. name 3. location 4. start date 5. duration");
		if (task.getType().equals(TaskTypeEnum.meeting.toString()))
			System.out.println(" 6. Add attendees 7. Remove attendee");

		switch (inputScanner.getInput()) {
		case "1":
			updateType(task);
			break;
		case "2":
			updateName(task);
			break;
		case "3":
			updateLocation(task);
			break;
		case "4":
			updateStartDate(task);
			break;
		case "5":
			updateDuration(task);
			break;
		case "7":
			removeAttendee(task);
			break;
		case "6":
			addAttendee(task);
		}

		tasksService.updateTask(task);
	}

	private void addAttendee(ToDoTask task) {
		addTaskStartMenuChoice.addAttendees(task);
	}

	private void removeAttendee(ToDoTask task) {
		System.out.println("Enter attendee ID you want to delete");
		((MeetingTask) task).removeAttendee(Integer.valueOf(inputScanner.getInput()));
	}

	private void updateDuration(ToDoTask task) {
		System.out.println(captionService.getCaption("menu.input.handler.add.task.duration"));
		task.setDuration(inputScanner.getInput());
	}

	private void updateStartDate(ToDoTask task) {
		System.out.println(captionService.getCaption("menu.input.handler.add.task.start.date"));
		task.setStartTime(inputScanner.getInput());
	}

	private void updateLocation(ToDoTask task) {
		System.out.println(captionService.getCaption("menu.input.handler.add.task.location"));
		task.setLocation(inputScanner.getInput());
	}

	private void updateName(ToDoTask task) {
		System.out.println(captionService.getCaption("menu.input.handler.add.task.name"));
		task.setName(inputScanner.getInput());
	}

	private void updateType(ToDoTask task) {
		System.out.println(captionService.getCaption("menu.input.handler.add.task.type"));
		task.setType(inputScanner.getInput());
	}

	@Override
	void executeSpecific() {
		updateTask();
	}
}
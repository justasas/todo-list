package main.startMenu;

import org.springframework.stereotype.Component;

@Component
public class DeleteTaskStartMenuChoice extends StartMenuChoice {

	@Override
	void executeSpecific() {
		System.out.println(captionService.getCaption("menu.input.handler.delete"));
		tasksService.deleteTask(Integer.valueOf(inputScanner.getInput()));
	}
}
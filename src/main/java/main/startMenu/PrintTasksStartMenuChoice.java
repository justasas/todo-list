package main.startMenu;

import java.util.Comparator;
import java.util.List;

import org.springframework.stereotype.Component;

import main.tasks.ToDoTask;

@Component
public class PrintTasksStartMenuChoice extends StartMenuChoice {

	@Override
	void executeSpecific() {
		System.out.println(
				"Choose how to print tasks: 1. print all 2. print sorted by name 3. to print sorted by start time");

		Comparator<ToDoTask> comp = null;

		String sortingChoice = inputScanner.getInput();

		if (sortingChoice.equals("1"))
			comp = null;

		if (sortingChoice.equals("2"))
			comp = ToDoTask.COMPARE_BY_NAME;

		if (sortingChoice.equals("3"))
			comp = ToDoTask.COMPARE_BY_START_TIME;

		System.out.println(captionService.getCaption("menu.input.handler.all.tasks"));

		final List<ToDoTask> tasks = tasksService.getTasks();
		if (comp != null) {
			List<ToDoTask> sortedTasks = tasksService.sortBy(comp, tasks);
			tasksService.printOutTasks(sortedTasks);
		} else {
			tasksService.printOutTasks(tasks);
		}
	}
}
package main.startMenu;

import org.springframework.beans.factory.annotation.Autowired;

import main.captions.CaptionService;
import main.data.InputScanner;
import main.tasks.TasksService;

public abstract class StartMenuChoice {

	@Autowired
	protected InputScanner inputScanner;

	@Autowired
	protected CaptionService captionService;

	@Autowired
	protected TasksService tasksService;

	public void execute() {
		executeSpecific();
	}

	abstract void executeSpecific();
}
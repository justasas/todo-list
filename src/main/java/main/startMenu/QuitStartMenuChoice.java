package main.startMenu;

import org.springframework.stereotype.Component;

@Component
public class QuitStartMenuChoice extends StartMenuChoice {

	@Override
	void executeSpecific() {
		System.exit(0);
	}
}
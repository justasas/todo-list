package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import main.tasks.TasksService;

@Component
public class Main implements CommandLineRunner {

	@Autowired
	private TasksService tasks;
	@Autowired
	private StartMenu startMenu;

	public static final Map<String, String> VARIABLES = new HashMap<String, String>();
	static {
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(Constants.VARIABLES_FILE_PATH)));

			String line;
			while ((line = br.readLine()) != null) {
				String[] keyValue = line.split(Constants.VARIABLES_FILE_KEY_TO_VALUE_SPLIT_SYMBOL);
				VARIABLES.put(keyValue[0], keyValue[1]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run(String... strings) throws Exception {

		Runnable mainThread = new Runnable() {
			public void run() {
				startMenu.start();

				System.exit(0);
			}
		};

		Runnable checkForReminder = new Runnable() {
			public void run() {
				tasks.checkForReminder();
			}
		};

		new Thread(mainThread).start();
		new Thread(checkForReminder).start();
	}

	public void loadData() {

	}
}
package main;

import java.text.SimpleDateFormat;

public class Constants {
	public final static String FILE_TASKS_NAME_KEY = "file.tasks.name";
	public final static String FILE_TASKS_DELIMETER = "file.tasks.delimeter";
	public final static String DATE_FORMAT_STRING = "yyyy-MM-dd HH:mm";
	public final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT_STRING);

	public final static String RESOURCES_DIR = "resources/";
	public final static String ITEMS_FILE_PATH = RESOURCES_DIR + "items.ser";
	public final static String TASKS_FILE_PATH = RESOURCES_DIR + "tasks.txt";
	public final static String VARIABLES_FILE_KEY_TO_VALUE_SPLIT_SYMBOL = "=";
	public final static String VARIABLES_FILE_PATH = RESOURCES_DIR + "variables.txt";

	public final static String REGEX_DURATION = "[0-9.]+";
}

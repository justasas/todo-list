package main.tasks;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import main.data.Repository;

@Service
public class TasksService {

	@Autowired
	private Repository repository;

	public TasksService() {

	}

	private boolean tasksOverlap(ToDoTask newTask, ToDoTask existingTask) {
		Date existingTaskEndTime = getEndTime(existingTask);
		Date newTaskEndTime = getEndTime(newTask);

		if (timesOverlap(existingTask.getStartTime(), existingTaskEndTime, newTask.getStartTime(), newTaskEndTime)) {
			System.out.println(
					"Task: ( " + newTask + " ) is not added because time overlaps with: " + existingTask + "\n");

			return true;
		}

		return false;
	}

	public List<ToDoTask> sortBy(Comparator comp, List<ToDoTask> tasks) {
		Collections.sort(tasks, comp);
		return tasks;
	}

	public List<ToDoTask> sortByStartTime(List<ToDoTask> tasks) {
		Collections.sort(tasks, ToDoTask.COMPARE_BY_START_TIME);
		return tasks;
	}

	public List<ToDoTask> sortByName(List<ToDoTask> tasks) {
		Collections.sort(tasks, ToDoTask.COMPARE_BY_NAME);
		return tasks;
	}

	public List<ToDoTask> findByType(String type) {
		List<ToDoTask> ret = new ArrayList<ToDoTask>();

		for (ToDoTask toDoTask : repository.getAllTasks()) {
			if (toDoTask.getType().equals(type))
				ret.add(toDoTask);
		}

		return ret;
	}

	protected Date getEndTime(ToDoTask existingTask) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(existingTask.getStartTime());
		String[] hoursMins = existingTask.getDuration().split("\\.");
		int hours = Integer.valueOf(hoursMins[0]);
		int mins = Integer.valueOf(hoursMins[1]);
		cal.add(Calendar.MINUTE, hours * 60 + mins);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	protected static boolean timesOverlap(Date start1, Date end1, Date start2, Date end2) {
		return !start1.after(end2) && !start2.after(end1);
	}

	public void printOutTasks(List<ToDoTask> list) {

		for (ToDoTask task : list) {
			System.out.println(task);
		}
	}

	public void deleteTask(ToDoTask toDoTask) {
		deleteTask(toDoTask.getId());
	}

	public void deleteTask(int id) {
		repository.removeTask(id);
	}

	public void checkForReminder() {
		try {
			while (true) {
				List<ToDoTask> tasks = repository.getAllTasks();
				Thread.sleep(3000);
				if (tasks != null)
					for (ToDoTask task : tasks) {
						if (task.getType().equals(TaskTypeEnum.reminder.toString())) {
							if (!task.isAlreadyNotified()) {
								long secs = (new Date().getTime() - task.getStartTime().getTime()) / 1000;
								int mins = (int) (secs / 60);
								if (mins <= 60) {
									System.out.println("you have reminder: " + task);
									task.setAlreadyNotified(true);
									repository.updateTask(task);
								}
							}
						}
					}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private boolean isAbleToAddReminderTask(ToDoTask newTask) {
		for (ToDoTask existingTask : repository.getAllTasks()) {

			if (newTask.getType().equals(TaskTypeEnum.reminder.toString())
					&& existingTask.getType().equals(TaskTypeEnum.reserveTime.toString()))
				if (tasksOverlap(newTask, existingTask))
					return false;
		}
		return true;
	}

	private boolean isAbleToAddMeetingTask(ToDoTask newTask) {
		for (ToDoTask existingTask : repository.getAllTasks()) {
			if (newTask.getType().equals(TaskTypeEnum.meeting.toString())
					&& (existingTask.getType().equals(TaskTypeEnum.meeting.toString())
							|| existingTask.getType().equals(TaskTypeEnum.reserveTime.toString())))
				if (tasksOverlap(newTask, existingTask))
					return false;
		}

		return true;
	}

	private boolean isAbleToAddReserveTimeTask(ToDoTask newTask) {
		for (ToDoTask existingTask : repository.getAllTasks()) {
			if (tasksOverlap(newTask, existingTask))
				return false;
		}

		return true;
	}

	public void addTask(ToDoTask newTask) {

		if (newTask == null)
			return;

		boolean enableToAdd = false;

		switch (TaskTypeEnum.valueOf(newTask.getType())) {
		case reminder:
			enableToAdd = isAbleToAddReminderTask(newTask);
			break;
		case meeting:
			enableToAdd = isAbleToAddMeetingTask(newTask);
			break;
		case reserveTime:
			enableToAdd = isAbleToAddReserveTimeTask(newTask);
			break;
		}

		if (enableToAdd) {
			repository.addTask(newTask);
		}
	}

	public ToDoTask getTask(int id) {
		return repository.getTask(id);
	}

	public List<ToDoTask> getTasks() {
		return repository.getAllTasks();
	}

	public void updateTask(ToDoTask task) {
		if (task != null)
			repository.updateTask(task);
	}
}
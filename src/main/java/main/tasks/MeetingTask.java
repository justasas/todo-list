package main.tasks;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MeetingTask extends ToDoTask {

	private List<Attendee> attendees = new ArrayList<Attendee>();

	@Override
	public String toString() {
		return super.toString() + " [attendees=" + attendees + "]";
	}

	public void setAttendees(List<Attendee> attendees) {
		this.attendees = attendees;
	}

	public MeetingTask(String type) {
		super(type);
	}

	public void removeAttendee(int id) {
		for (Iterator<Attendee> iter = attendees.listIterator(); iter.hasNext();) {
			Attendee a = iter.next();
			if (a.getId() == id) {
				iter.remove();
				break;
			}
		}
	}

	public List<Attendee> getAttendees() {
		return attendees;
	}

	public void addAttendee(Attendee attendee) {
		attendees.add(attendee);
	}

}
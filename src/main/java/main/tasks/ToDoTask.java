package main.tasks;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import main.Utils;

public abstract class ToDoTask implements Serializable {

	private int id;
	private String name;
	private Date startTime;
	private String duration;
	private String location;
	private String type;
	private boolean alreadyNotified;

	// public final static String REMINDER_TYPE_NAME = "reminder";
	// public final static String MEETING_TYPE_NAME = "meeting";
	// public final static String RESERVE_TIME_TYPE_NAME = "reserveTime";

	public ToDoTask() {

	}

	public ToDoTask(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public void setStartTime(String date) {
		this.startTime = Utils.parseDate(date);
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public static ToDoTask CREATE(String type) {

		ToDoTask toDoTask = null;

		switch (TaskTypeEnum.valueOf(type)) {
		case reminder:
			toDoTask = new ReminderTask(type);
			break;
		case meeting:
			toDoTask = new MeetingTask(type);
			break;
		case reserveTime:
			toDoTask = new ReserveTimeTask(type);
			break;
		}

		return toDoTask;
	}

	public static ToDoTask CREATE(int id, String type, String name, String duration, Date startTime, String location,
			boolean alreadyNotified) {
		ToDoTask toDoTask = CREATE(type);
		toDoTask.setId(id);
		toDoTask.setName(name);
		toDoTask.setDuration(duration);
		toDoTask.setLocation(location);
		toDoTask.setStartTime(startTime);
		toDoTask.setAlreadyNotified(alreadyNotified);
		return toDoTask;
	}

	public void setId(int id) {
		this.id = id;
	}

	public static Comparator<ToDoTask> COMPARE_BY_NAME = new Comparator<ToDoTask>() {
		@Override
		public int compare(ToDoTask one, ToDoTask other) {
			return one.name.compareTo(other.name);
		}
	};

	public static Comparator<ToDoTask> COMPARE_BY_START_TIME = new Comparator<ToDoTask>() {
		@Override
		public int compare(ToDoTask one, ToDoTask other) {
			return one.startTime.compareTo(other.startTime);
		}
	};

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isAlreadyNotified() {
		return alreadyNotified;
	}

	public void setAlreadyNotified(boolean alreadyNotified) {
		this.alreadyNotified = alreadyNotified;
	}

	@Override
	public String toString() {
		return "ToDoTask [id=" + id + ", name=" + name + ", startTime=" + startTime + ", duration=" + duration
				+ ", location=" + location + ", type=" + type + ", alreadyNotified=" + alreadyNotified + "]";
	}

}
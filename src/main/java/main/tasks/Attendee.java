package main.tasks;

public class Attendee {

	public Attendee() {

	}

	private int id;

	@Override
	public String toString() {
		return "Attendee [id=" + id + ", name=" + name + "]";
	}

	private String name;

	public Attendee(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean equals(Attendee object2) {
		return this.name.equals(object2.name);
	}
}
package main.tasks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import main.Constants;
import main.Utils;

@Controller
public class TasksController {

	@Autowired
	private TasksService tasksService;

	@ModelAttribute("task")
	public TaskForm getTask() {
		return new TaskForm();
	}

	@RequestMapping("/task/{id}")
	public String viewTask(@PathVariable int id, Model model) {
		ToDoTask task = tasksService.getTask(id);
		model.addAttribute("task", task);
		if (task.getType().equals(TaskTypeEnum.meeting.toString()))
			model.addAttribute("attendees", ((MeetingTask) task).getAttendees());
		else {
			model.addAttribute("attendees", new ArrayList<Attendee>());
		}
		return "task/view";
	}

	@RequestMapping(value = "/task/new", method = RequestMethod.GET)
	public String newTaskPage(Model model) {
		model.addAttribute("formAction", "/task/new");
		return "task/edit";
	}

	@RequestMapping(value = { "/task/new" }, method = RequestMethod.POST)
	public String addNewPost(@Valid TaskForm taskForm, BindingResult result, Model model) {

		if (result.hasErrors()) {
			model.addAttribute("task", taskForm);
			model.addAttribute("exception", result.toString());
			return "task/edit";
		}

		ToDoTask task = convertTaskFormToTask(taskForm);

		tasksService.addTask(task);

		return "redirect:/";
	}

	@RequestMapping(value = { "/task/update" }, method = RequestMethod.POST)
	public String updateNewPost(@Valid TaskForm taskForm, BindingResult result, Model model) {

		// if (!taskForm.getDuration().matches(Constants.REGEX_DURATION)) {
		// throws new ControllerItemInputValidationException();
		// }
		if (result.hasErrors()) {
			model.addAttribute("task", taskForm);
			model.addAttribute("exception", result.toString());
			return "task/edit";
		}

		ToDoTask task = convertTaskFormToTask(taskForm);

		tasksService.updateTask(task);

		return "redirect:/";
	}

	private List<Attendee> removeEmptyAttendees(Attendee[] attendees) {

		List<Attendee> ret = new LinkedList<Attendee>(Arrays.asList(attendees));

		for (Iterator<Attendee> itr = ret.iterator(); itr.hasNext();) {
			final Attendee next = itr.next();
			if (next == null || next.getName().isEmpty()) {
				itr.remove();
			}
		}
		return ret;
	}

	@RequestMapping("/task/delete/{id}")
	public String deleteTask(@PathVariable int id) {
		tasksService.deleteTask(id);
		return "redirect:/";
	}

	@RequestMapping("/task/update/{id}")
	public String updateTask(@PathVariable int id, Model model) {

		TaskForm taskForm = convertTaskToTaskForm(tasksService.getTask(id));
		model.addAttribute("task", taskForm);
		model.addAttribute("formAction", "/task/update");

		return "task/edit";
	}

	@RequestMapping("/task/sortByName")
	public String sortByName(Model model) {

		List<ToDoTask> sortedTasks = tasksService.sortByName(tasksService.getTasks());
		model.addAttribute("allTasks", sortedTasks);

		return "index";
	}

	@RequestMapping("/task/sortByStartTime")
	public String sortByStartTime(Model model) {

		List<ToDoTask> sortedTasks = tasksService.sortByStartTime(tasksService.getTasks());
		model.addAttribute("allTasks", sortedTasks);

		return "index";
	}

	private TaskForm convertTaskToTaskForm(ToDoTask task) {

		TaskForm taskForm = new TaskForm(task.getId(), task.getName(),
				Constants.DATE_FORMAT.format(task.getStartTime()), task.getDuration(), task.getLocation(),
				task.getType(), task.isAlreadyNotified());

		Attendee[] attendeesArray = new Attendee[10];

		if (task.getType().equals(TaskTypeEnum.meeting.toString())) {
			int i = 0;
			for (Attendee a : ((MeetingTask) task).getAttendees()) {
				attendeesArray[i++] = a;
			}
			taskForm.setAttendees(attendeesArray);
		} else {
			taskForm.setAttendees(attendeesArray);
		}

		return taskForm;
	}

	private ToDoTask convertTaskFormToTask(TaskForm taskForm) {
		ToDoTask task = ToDoTask.CREATE(0, taskForm.getType(), taskForm.getName(), taskForm.getDuration(),
				Utils.parseDate(taskForm.getStartTime()), taskForm.getLocation(), false);

		if (task != null) {

			final String type = taskForm.getType();

			if (type.equals(TaskTypeEnum.meeting.toString())) {
				List<Attendee> attendees = removeEmptyAttendees(taskForm.getAttendees());
				((MeetingTask) task).setAttendees(attendees);
			}
		}
		return task;
	}
}
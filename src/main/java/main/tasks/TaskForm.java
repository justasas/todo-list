package main.tasks;

import org.hibernate.validator.constraints.NotEmpty;

public class TaskForm {

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isAlreadyNotified() {
		return alreadyNotified;
	}

	public void setAlreadyNotified(boolean alreadyNotified) {
		this.alreadyNotified = alreadyNotified;
	}

	public Attendee[] getAttendees() {
		return attendees;
	}

	public void setAttendees(Attendee[] attendees) {
		this.attendees = attendees;
	}

	private int id;
	@NotEmpty
	private String name;
	@NotEmpty
	private String startTime;
	@NotEmpty
	private String duration;
	@NotEmpty
	private String location;
	@NotEmpty
	private String type;
	private boolean alreadyNotified;

	public TaskForm(int id, String name, String startTime, String duration, String location, String type,
			boolean alreadyNotified) {
		super();
		this.id = id;
		this.name = name;
		this.startTime = startTime;
		this.duration = duration;
		this.location = location;
		this.type = type;
		this.alreadyNotified = alreadyNotified;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public TaskForm() {

	}

	private Attendee[] attendees = new Attendee[10];
}
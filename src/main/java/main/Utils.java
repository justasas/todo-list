package main;

import java.util.Calendar;
import java.util.Date;

public class Utils {

	public static Date parseDate(String userInputDate) {
		String yearMonthDay = userInputDate.split(" ")[0];
		String[] yearMonthDayArray = yearMonthDay.split("-");
		String hourMinutes = userInputDate.split(" ")[1];

		int year = Integer.valueOf(yearMonthDayArray[0]);
		int month = Integer.valueOf(yearMonthDayArray[1]);
		int day = Integer.valueOf(yearMonthDayArray[2]);

		int hrs = Integer.valueOf(hourMinutes.split(":")[0]);
		int min = Integer.valueOf(hourMinutes.split(":")[1]);

		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, day, hrs, min);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
}

package main.controllers.advice;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import main.controllers.exceptions.ControllerItemInputValidationException;

@ControllerAdvice
public class ExceptionHandlingControllerAdvice {

	@ExceptionHandler(ControllerItemInputValidationException.class)
	public ModelAndView handleItemUpdateValidationException(Exception ex, Model model) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView = modelAndView.addAllObjects(model.asMap());
		modelAndView.addObject("exception", ex.getMessage());
		modelAndView.setViewName("task/edit");
		return modelAndView;
	}
}

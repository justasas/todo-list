package main;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import main.tasks.TaskTypeEnum;
import main.tasks.TasksService;
import main.tasks.ToDoTask;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class TasksTest {

	@Autowired
	TasksService tasks;

	Date date1;
	Date date2;
	Date date3;
	Date date4;

	@Before
	public void setUp() {
		try {
			for (ToDoTask toDoTask : tasks.getTasks()) {
				tasks.deleteTask(toDoTask);
			}

			date1 = Constants.DATE_FORMAT.parse("2016-07-29 15:30");
			date2 = Constants.DATE_FORMAT.parse("2016-07-28 15:30");
			date3 = Constants.DATE_FORMAT.parse("2016-08-15 15:30");
			date4 = Constants.DATE_FORMAT.parse("2016-08-15 15:40");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testSortByStartTime() {

		ToDoTask toDoTask1 = ToDoTask.CREATE(0, TaskTypeEnum.meeting.toString(), "name2", "1.10", date1, "location1",
				false);
		ToDoTask toDoTask2 = ToDoTask.CREATE(1, TaskTypeEnum.meeting.toString(), "name1", "2.20", date2, "location2",
				false);

		tasks.addTask(toDoTask1);
		tasks.addTask(toDoTask2);

		List<ToDoTask> sortedTasks = tasks.sortByStartTime(tasks.getTasks());

		assertEquals(sortedTasks.get(0).getName(), toDoTask2.getName());
		assertEquals(sortedTasks.get(1).getName(), toDoTask1.getName());
	}

	@Test
	public void testSortByName() {

		ToDoTask toDoTask1 = ToDoTask.CREATE(0, TaskTypeEnum.meeting.toString(), "b", "1.10", date1, "location1",
				false);
		ToDoTask toDoTask2 = ToDoTask.CREATE(1, TaskTypeEnum.meeting.toString(), "a", "2.20", date2, "location2",
				false);

		tasks.addTask(toDoTask1);
		tasks.addTask(toDoTask2);

		List<ToDoTask> sortedTasks = tasks.sortByName(tasks.getTasks());
		assertEquals(toDoTask1.getName(), sortedTasks.get(1).getName());
		assertEquals(toDoTask2.getName(), sortedTasks.get(0).getName());
		// assertThat(tasks.getTasks(),
		// contains(hasProperty("name", org.hamcrest.core.Is("a")),
		// hasProperty("name", is("b"))));
	}

	@Test
	public void testFindByType() {
		// given
		ToDoTask toDoTask1 = ToDoTask.CREATE(0, TaskTypeEnum.meeting.toString(), "name2", "1.10", date1, "location1",
				false);
		ToDoTask toDoTask2 = ToDoTask.CREATE(1, TaskTypeEnum.reserveTime.toString(), "name1", "2.20", date2,
				"location2", false);

		tasks.addTask(toDoTask1);
		tasks.addTask(toDoTask2);

		// when
		List<ToDoTask> tasksByType = tasks.findByType(toDoTask1.getType());

		// then
		assertEquals(tasksByType.get(0).getName(), toDoTask1.getName());
	}

	@Test
	public void testDeleteTaskToDoTask() {
		ToDoTask toDoTask1 = ToDoTask.CREATE(0, TaskTypeEnum.meeting.toString(), "name1", "1.10", date1, "location1",
				false);
		ToDoTask toDoTask2 = ToDoTask.CREATE(1, TaskTypeEnum.meeting.toString(), "name2", "2.20", date2, "location2",
				false);

		tasks.addTask(toDoTask1);
		tasks.addTask(toDoTask2);
		tasks.deleteTask(toDoTask1);

		assertEquals(toDoTask2.getName(), tasks.getTasks().get(0).getName());
	}

	@Test
	public void testDeleteTaskInt() {
		ToDoTask toDoTask1 = ToDoTask.CREATE(0, TaskTypeEnum.meeting.toString(), "name1", "1.10", date1, "location1",
				false);
		ToDoTask toDoTask2 = ToDoTask.CREATE(1, TaskTypeEnum.meeting.toString(), "name2", "2.20", date2, "location2",
				false);

		tasks.addTask(toDoTask1);
		tasks.addTask(toDoTask2);
		tasks.deleteTask(0);

		assertEquals(toDoTask2.getName(), tasks.getTasks().get(0).getName());
	}

	// Meetings can't overlap with other meetings.
	@Test
	public void shouldNotAddMeetingWhichOverlapsWithOtherMeeting() {

		ToDoTask toDoTask3 = ToDoTask.CREATE(0, TaskTypeEnum.meeting.toString(), "name3", "2.20", date3, "location3",
				false);
		ToDoTask toDoTask4 = ToDoTask.CREATE(1, TaskTypeEnum.meeting.toString(), "name4", "2.20", date4, "location4",
				false);

		tasks.addTask(toDoTask3);
		tasks.addTask(toDoTask4);

		assertThat(tasks.getTasks(), not(contains(toDoTask4)));
	}

	// **************************************
	// No reminders or meetings can overlap with reserved time.
	@Test
	public void shouldNotAddMeetingWhichOverlapsWithReservedTime() {

		ToDoTask toDoTask3 = ToDoTask.CREATE(0, TaskTypeEnum.reserveTime.toString(), "name3", "2.20", date3,
				"location3", false);
		ToDoTask toDoTask4 = ToDoTask.CREATE(1, TaskTypeEnum.meeting.toString(), "name4", "2.20", date4, "location4",
				false);

		tasks.addTask(toDoTask3);
		tasks.addTask(toDoTask4);

		assertThat(tasks.getTasks(), not(contains(toDoTask4)));
	}

	@Test
	public void testShouldNotAddReminderWhichOverlapsReserveTime() {

		ToDoTask toDoTask3 = ToDoTask.CREATE(0, TaskTypeEnum.reserveTime.toString(), "name3", "2.20", date3,
				"location3", false);
		ToDoTask toDoTask4 = ToDoTask.CREATE(1, TaskTypeEnum.reserveTime.toString(), "name4", "2.20", date4,
				"location4", false);

		tasks.addTask(toDoTask3);
		tasks.addTask(toDoTask4);

		assertThat(tasks.getTasks(), not(contains(toDoTask4)));
	}

	// *****************************************

	// ***************************
	// testShouldNotAddReserveTimeIfItOverlapsWithAnotherTask
	@Test
	public void testShouldNotAddReserveTimeIfItOverlapsWithReminderTask() {

		ToDoTask toDoTask3 = ToDoTask.CREATE(0, TaskTypeEnum.reserveTime.toString(), "name3", "2.20", date3,
				"location3", false);
		ToDoTask toDoTask4 = ToDoTask.CREATE(1, TaskTypeEnum.reserveTime.toString(), "name4", "2.20", date4,
				"location4", false);

		tasks.addTask(toDoTask3);
		tasks.addTask(toDoTask4);

		assertThat(tasks.getTasks(), not(contains(toDoTask4)));
	}

	@Test
	public void testShouldNotAddReserveTimeIfItOverlapsWithMeetingTask() {

		ToDoTask toDoTask3 = ToDoTask.CREATE(0, TaskTypeEnum.meeting.toString(), "name3", "2.20", date3, "location3",
				false);
		ToDoTask toDoTask4 = ToDoTask.CREATE(1, TaskTypeEnum.reserveTime.toString(), "name4", "2.20", date4,
				"location4", false);

		tasks.addTask(toDoTask3);
		tasks.addTask(toDoTask4);

		assertThat(tasks.getTasks(), not(contains(toDoTask4)));
	}

	@Test
	public void testShouldNotAddReserveTimeIfItOverlapsWithReserveTimeTask() {

		ToDoTask toDoTask3 = ToDoTask.CREATE(0, TaskTypeEnum.reserveTime.toString(), "name3", "2.20", date3,
				"location3", false);
		ToDoTask toDoTask4 = ToDoTask.CREATE(1, TaskTypeEnum.reserveTime.toString(), "name4", "2.20", date4,
				"location4", false);

		tasks.addTask(toDoTask3);
		tasks.addTask(toDoTask4);

		assertThat(tasks.getTasks(), not(contains(toDoTask4)));
	}

	// ******************************
}
